﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

public static class MyEnumerable
	{
		public static BindingList<TElement> ToBindingList<TElement>(this IEnumerable<TElement> seq)
		{
			return new BindingList<TElement>(seq.ToList());
		}

		public static void ForEachDo<TElement>(this IEnumerable<TElement> seq, Action<TElement> action)
		{
			Debug.Assert(action != null, "action != null");
			foreach(TElement element in seq)
			{
				action( element );
			}
		}

		public static IEnumerable<TElement> DistinctByKey<TElement, TKey>(this IEnumerable<TElement> seq, Func<TElement, TKey> keySelector)
		{
			Debug.Assert(keySelector != null, "keySelector != null");

			HashSet<TKey> keySet = new HashSet<TKey>();

			foreach(TElement element in seq)
			{
				TKey key = keySelector( element );
				if(keySet.Add( key ))
				{
					yield return element;
				}
			}
		}

		public static SortedDictionary<TKey, TValue> ToSortedDictionary<TKey, TValue, TSrc>(this IEnumerable<TSrc> enumerable, Func<TSrc, TKey> keySelector, Func<TSrc, TValue> valueSelector)
		{
			return ToIDictionary<SortedDictionary<TKey, TValue>, TKey, TValue, TSrc>( enumerable, keySelector, valueSelector);
		}

		public static SortedList<TKey, TValue> ToSortedList<TKey, TValue, TSrc>(this IEnumerable<TSrc> enumerable, Func<TSrc, TKey> keySelector, Func<TSrc, TValue> valueSelector)
		{
			return ToIDictionary<SortedList<TKey, TValue>, TKey, TValue, TSrc>( enumerable, keySelector, valueSelector );
		}

		private static TDict ToIDictionary<TDict, TKey, TValue, TSrc>(this IEnumerable<TSrc> enumerable, Func<TSrc, TKey> keySelector, Func<TSrc, TValue> valueSelector) where TDict:IDictionary<TKey, TValue>, new()
		{
			Debug.Assert(enumerable != null, "enumerable != null");
			Debug.Assert(keySelector != null, "keySelector != null");
			Debug.Assert(valueSelector != null, "valueSelector != null");

			var dict = new TDict();
			foreach(TSrc src in enumerable)
			{
				TKey key = keySelector(src);
				TValue value = valueSelector(src);
				dict.Add(key, value);
			}

			return dict;
		}

	}
