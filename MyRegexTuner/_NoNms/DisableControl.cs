﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;


public class DisableControl :IDisposable
{
	readonly IDictionary<Control, bool> prewStateStorage = new Dictionary<Control, bool>();

	public DisableControl(params Control[] controls)
	{
		foreach(var control in controls)
		{
			Control ctrl = control;
			if(control.InvokeRequired)
			{
				control.Invoke(
					(EventHandler)delegate
					              {
					              	SetControlDisabled( ctrl );
								  }
					);
			}
			else
				SetControlDisabled( control );
		}
	}

	private void SetControlDisabled(Control control)
	{
		prewStateStorage[control] = control.Enabled;
		control.Enabled = false;
	}

	#region Dispose

	private bool bDisposed = false;
	public void Dispose()
	{
		if(!bDisposed)
		{
			foreach(var pair in prewStateStorage)
			{
				Control ctrl = pair.Key;
				bool state = pair.Value;

				if(ctrl.InvokeRequired)
				{
					ctrl.Invoke(
						(EventHandler)delegate { ctrl.Enabled = state; }
						);
				}
				else
					ctrl.Enabled = state;
			}

			bDisposed = true;
		}
	}

	#endregion

	public static DisableControl Instance(params Control[] controls)
	{
		return new DisableControl(controls);
	}
}