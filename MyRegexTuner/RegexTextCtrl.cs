﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using MyRegexTuner.Gui;
using MyRegexTuner.Storage;

namespace MyRegexTuner
{
	public partial class RegexTextCtrl :UserControl
	{
		public RegexTextCtrl()
		{
			InitializeComponent();
			this.multiTabTextCtrl.TextChanged += multiTabTextCtrl_TextChanged;
		}

		
		[Browsable(true)]
		public new event EventHandler TextChanged;

		public bool IsRegexValid { get; private set; }

		private void RaiseTextChanged(object sender, EventArgs e)
		{
			if(this.TextChanged != null)
				this.TextChanged(this, EventArgs.Empty);
		}

		[Browsable(true)]
		public string HeaderText
		{
			get
			{
				return multiTabTextCtrl.HeaderText;
			}
			set
			{
				multiTabTextCtrl.HeaderText = value;
			}
		}

		public new string Text
		{
			get
			{
				return this.multiTabTextCtrl.Text;
			}
		}

		public string TabPrefix
		{
			get
			{
				return this.multiTabTextCtrl.TabPrefix;
			}
			set
			{
				this.multiTabTextCtrl.TabPrefix = value;
			}
		}

		public void AddNewPage()
		{
			this.multiTabTextCtrl.AddNewPage();
		}

		private void multiTabTextCtrl_TextChanged(object sender, EventArgs e)
		{
			TestRegexPattern();
			RaiseTextChanged(this, EventArgs.Empty);
		}

		private void TestRegexPattern()
		{
			try
			{
				Regex rx = new Regex( this.multiTabTextCtrl.Text, this.regexStyleCtrl.RegexOptions );

				this.lblStatus.Text = "Ok!";
				this.lblStatus.ForeColor = SystemColors.ControlText;
				this.lblStatus.BackColor = SystemColors.Control;
				this.IsRegexValid = true;
			}
			catch(ArgumentException)
			{
				this.lblStatus.Text = "ERROR!";
				this.lblStatus.ForeColor = Color.Yellow;
				this.lblStatus.BackColor = Color.Red;
				this.IsRegexValid = false;
			}
		}

		public Regex GetRegexObject()
		{
			try
			{
				return new Regex( this.multiTabTextCtrl.Text, this.regexStyleCtrl.RegexOptions );
			}
			catch(ArgumentException)
			{
				return null;
			}
		}

		public void SetInitialFromStorage(RegexStorage storage)
		{
			RegexOptions options = (RegexOptions)Enum.Parse( typeof(RegexOptions), storage.options );
			this.regexStyleCtrl.RegexOptions = options;

			this.multiTabTextCtrl.SetInitialFromStorage( storage.regex );
		}

		public RegexStorage GetCurrentStateForStorage()
		{
			RegexStorage storage = new RegexStorage();
			storage.regex = this.multiTabTextCtrl.GetCurrentStateForStorage();
			storage.options = this.regexStyleCtrl.RegexOptions.ToString();

			return storage;
		}

		private void regexStyleCtrl_RegexStyleChanged(object sender, EventArgs e)
		{
			TestRegexPattern();
			RaiseTextChanged(sender, e);
		}
	}
}
