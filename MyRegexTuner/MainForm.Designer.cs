﻿namespace MyRegexTuner
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Panel pnlButtons;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnSplit2 = new System.Windows.Forms.Button();
            this.btnDoMatch2 = new System.Windows.Forms.Button();
            this.btnIsMatch2 = new System.Windows.Forms.Button();
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.btnNewPattern = new System.Windows.Forms.ToolStripButton();
            this.btnNewText = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnIsMatch = new System.Windows.Forms.ToolStripButton();
            this.btnDoMatch = new System.Windows.Forms.ToolStripButton();
            this.btnSplit = new System.Windows.Forms.ToolStripButton();
            this.btnReplace = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnExit = new System.Windows.Forms.ToolStripButton();
            this.pnlInput = new System.Windows.Forms.Panel();
            this.pnlSrcText = new System.Windows.Forms.Panel();
            this.ctrlInput = new MyRegexTuner.MultiTabTextCtrl();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.ctrlRegex = new MyRegexTuner.RegexTextCtrl();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.pnlGroups = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lstGroupsToSelect = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.pnlOutput = new System.Windows.Forms.Panel();
            this.tabControlResult = new System.Windows.Forms.TabControl();
            this.tabPageMatch = new System.Windows.Forms.TabPage();
            this.gboxMatches = new System.Windows.Forms.GroupBox();
            this.lstMatches = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStripForCopy = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuCopyValue = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuCopyAsTabbedText = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCopyAsCsv = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuSaveAsCsvSysDefault = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSaveAsCsvUtf8 = new System.Windows.Forms.ToolStripMenuItem();
            this.splitterGroups = new System.Windows.Forms.Splitter();
            this.pnlMatchDetail = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lstCaptures = new System.Windows.Forms.ListView();
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.statusStripForCaptures = new System.Windows.Forms.StatusStrip();
            this.lblCaptureCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblCapturePosition = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lstGroups = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblShowHideButton = new System.Windows.Forms.Label();
            this.tabPageReplace = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtReplaceResult = new System.Windows.Forms.RichTextBox();
            this.contextMenuOnlyCopy = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuCopyReplace = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnReplace2 = new System.Windows.Forms.Button();
            this.txtReplacePattern = new System.Windows.Forms.TextBox();
            this.tabPageSplit = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lstSlit = new System.Windows.Forms.ListView();
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.splitter6 = new System.Windows.Forms.Splitter();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtSplitElement = new System.Windows.Forms.RichTextBox();
            this.tabPageIsMatch = new System.Windows.Forms.TabPage();
            this.lblIsMatchResult = new System.Windows.Forms.Label();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.sfd = new System.Windows.Forms.SaveFileDialog();
            pnlButtons = new System.Windows.Forms.Panel();
            pnlButtons.SuspendLayout();
            this.mainToolStrip.SuspendLayout();
            this.pnlInput.SuspendLayout();
            this.pnlSrcText.SuspendLayout();
            this.pnlGroups.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.pnlOutput.SuspendLayout();
            this.tabControlResult.SuspendLayout();
            this.tabPageMatch.SuspendLayout();
            this.gboxMatches.SuspendLayout();
            this.contextMenuStripForCopy.SuspendLayout();
            this.pnlMatchDetail.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.statusStripForCaptures.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPageReplace.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.contextMenuOnlyCopy.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabPageSplit.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tabPageIsMatch.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlButtons
            // 
            pnlButtons.Controls.Add(this.btnSplit2);
            pnlButtons.Controls.Add(this.btnDoMatch2);
            pnlButtons.Controls.Add(this.btnIsMatch2);
            pnlButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            pnlButtons.Location = new System.Drawing.Point(0, 206);
            pnlButtons.Name = "pnlButtons";
            pnlButtons.Size = new System.Drawing.Size(200, 113);
            pnlButtons.TabIndex = 0;
            // 
            // btnSplit2
            // 
            this.btnSplit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSplit2.Location = new System.Drawing.Point(9, 78);
            this.btnSplit2.Name = "btnSplit2";
            this.btnSplit2.Size = new System.Drawing.Size(182, 27);
            this.btnSplit2.TabIndex = 2;
            this.btnSplit2.Text = "F7 - Split();";
            this.btnSplit2.UseVisualStyleBackColor = true;
            this.btnSplit2.Click += new System.EventHandler(this.btnSplit_Click);
            // 
            // btnDoMatch2
            // 
            this.btnDoMatch2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDoMatch2.Location = new System.Drawing.Point(9, 8);
            this.btnDoMatch2.Name = "btnDoMatch2";
            this.btnDoMatch2.Size = new System.Drawing.Size(182, 27);
            this.btnDoMatch2.TabIndex = 0;
            this.btnDoMatch2.Text = "F5 - Matches();";
            this.btnDoMatch2.UseVisualStyleBackColor = true;
            this.btnDoMatch2.Click += new System.EventHandler(this.btnDoMatch_Click);
            // 
            // btnIsMatch2
            // 
            this.btnIsMatch2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIsMatch2.Location = new System.Drawing.Point(9, 43);
            this.btnIsMatch2.Name = "btnIsMatch2";
            this.btnIsMatch2.Size = new System.Drawing.Size(182, 27);
            this.btnIsMatch2.TabIndex = 1;
            this.btnIsMatch2.Text = "F4 - IsMatch();";
            this.btnIsMatch2.UseVisualStyleBackColor = true;
            this.btnIsMatch2.Click += new System.EventHandler(this.btnIsMatch_Click);
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNewPattern,
            this.btnNewText,
            this.toolStripSeparator2,
            this.btnIsMatch,
            this.btnDoMatch,
            this.btnSplit,
            this.btnReplace,
            this.toolStripSeparator1,
            this.btnHelp,
            this.toolStripSeparator3,
            this.btnExit});
            this.mainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Size = new System.Drawing.Size(979, 25);
            this.mainToolStrip.TabIndex = 0;
            this.mainToolStrip.Text = "toolStrip1";
            // 
            // btnNewPattern
            // 
            this.btnNewPattern.Image = ((System.Drawing.Image)(resources.GetObject("btnNewPattern.Image")));
            this.btnNewPattern.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNewPattern.Name = "btnNewPattern";
            this.btnNewPattern.Size = new System.Drawing.Size(82, 22);
            this.btnNewPattern.Text = "New &regex";
            this.btnNewPattern.Click += new System.EventHandler(this.btnNewPattern_Click);
            // 
            // btnNewText
            // 
            this.btnNewText.Image = ((System.Drawing.Image)(resources.GetObject("btnNewText.Image")));
            this.btnNewText.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNewText.Name = "btnNewText";
            this.btnNewText.Size = new System.Drawing.Size(73, 22);
            this.btnNewText.Text = "New &text";
            this.btnNewText.Click += new System.EventHandler(this.btnNewText_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnIsMatch
            // 
            this.btnIsMatch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnIsMatch.Image = ((System.Drawing.Image)(resources.GetObject("btnIsMatch.Image")));
            this.btnIsMatch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnIsMatch.Name = "btnIsMatch";
            this.btnIsMatch.Size = new System.Drawing.Size(64, 22);
            this.btnIsMatch.Text = "&IsMatch();";
            this.btnIsMatch.Click += new System.EventHandler(this.btnIsMatch_Click);
            // 
            // btnDoMatch
            // 
            this.btnDoMatch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDoMatch.Image = ((System.Drawing.Image)(resources.GetObject("btnDoMatch.Image")));
            this.btnDoMatch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDoMatch.Name = "btnDoMatch";
            this.btnDoMatch.Size = new System.Drawing.Size(67, 22);
            this.btnDoMatch.Text = "&Matches();";
            this.btnDoMatch.Click += new System.EventHandler(this.btnDoMatch_Click);
            // 
            // btnSplit
            // 
            this.btnSplit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnSplit.Image = ((System.Drawing.Image)(resources.GetObject("btnSplit.Image")));
            this.btnSplit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSplit.Name = "btnSplit";
            this.btnSplit.Size = new System.Drawing.Size(45, 22);
            this.btnSplit.Text = "&Split();";
            this.btnSplit.Click += new System.EventHandler(this.btnSplit_Click);
            // 
            // btnReplace
            // 
            this.btnReplace.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnReplace.Image = ((System.Drawing.Image)(resources.GetObject("btnReplace.Image")));
            this.btnReplace.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnReplace.Name = "btnReplace";
            this.btnReplace.Size = new System.Drawing.Size(63, 22);
            this.btnReplace.Text = "Replace();";
            this.btnReplace.Click += new System.EventHandler(this.btnReplace_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnHelp
            // 
            this.btnHelp.CheckOnClick = true;
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(68, 22);
            this.btnHelp.Text = "Regex help";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btnExit
            // 
            this.btnExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(87, 22);
            this.btnExit.Text = ">>-- E&xit -->>";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlInput
            // 
            this.pnlInput.Controls.Add(this.pnlSrcText);
            this.pnlInput.Controls.Add(this.splitter2);
            this.pnlInput.Controls.Add(this.pnlGroups);
            this.pnlInput.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlInput.Location = new System.Drawing.Point(0, 25);
            this.pnlInput.Name = "pnlInput";
            this.pnlInput.Size = new System.Drawing.Size(979, 319);
            this.pnlInput.TabIndex = 1;
            // 
            // pnlSrcText
            // 
            this.pnlSrcText.Controls.Add(this.ctrlInput);
            this.pnlSrcText.Controls.Add(this.splitter3);
            this.pnlSrcText.Controls.Add(this.ctrlRegex);
            this.pnlSrcText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSrcText.Location = new System.Drawing.Point(0, 0);
            this.pnlSrcText.Name = "pnlSrcText";
            this.pnlSrcText.Size = new System.Drawing.Size(776, 319);
            this.pnlSrcText.TabIndex = 2;
            // 
            // ctrlInput
            // 
            this.ctrlInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlInput.HeaderText = "Text for input";
            this.ctrlInput.Location = new System.Drawing.Point(0, 149);
            this.ctrlInput.Name = "ctrlInput";
            this.ctrlInput.Size = new System.Drawing.Size(776, 170);
            this.ctrlInput.TabIndex = 1;
            this.ctrlInput.TabPrefix = "Text";
            // 
            // splitter3
            // 
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Location = new System.Drawing.Point(0, 146);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(776, 3);
            this.splitter3.TabIndex = 1;
            this.splitter3.TabStop = false;
            // 
            // ctrlRegex
            // 
            this.ctrlRegex.Dock = System.Windows.Forms.DockStyle.Top;
            this.ctrlRegex.HeaderText = "Regular expression";
            this.ctrlRegex.Location = new System.Drawing.Point(0, 0);
            this.ctrlRegex.Name = "ctrlRegex";
            this.ctrlRegex.Size = new System.Drawing.Size(776, 146);
            this.ctrlRegex.TabIndex = 0;
            this.ctrlRegex.TabPrefix = "Regex";
            this.ctrlRegex.TextChanged += new System.EventHandler(this.ctrlRegex_TextChanged);
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(776, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 319);
            this.splitter2.TabIndex = 1;
            this.splitter2.TabStop = false;
            // 
            // pnlGroups
            // 
            this.pnlGroups.Controls.Add(this.groupBox1);
            this.pnlGroups.Controls.Add(pnlButtons);
            this.pnlGroups.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlGroups.Location = new System.Drawing.Point(779, 0);
            this.pnlGroups.Name = "pnlGroups";
            this.pnlGroups.Size = new System.Drawing.Size(200, 319);
            this.pnlGroups.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lstGroupsToSelect);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 206);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Groups";
            // 
            // lstGroupsToSelect
            // 
            this.lstGroupsToSelect.CheckBoxes = true;
            this.lstGroupsToSelect.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lstGroupsToSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstGroupsToSelect.FullRowSelect = true;
            this.lstGroupsToSelect.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstGroupsToSelect.Location = new System.Drawing.Point(3, 16);
            this.lstGroupsToSelect.MultiSelect = false;
            this.lstGroupsToSelect.Name = "lstGroupsToSelect";
            this.lstGroupsToSelect.Size = new System.Drawing.Size(194, 187);
            this.lstGroupsToSelect.TabIndex = 0;
            this.lstGroupsToSelect.UseCompatibleStateImageBehavior = false;
            this.lstGroupsToSelect.View = System.Windows.Forms.View.Details;
            this.lstGroupsToSelect.SizeChanged += new System.EventHandler(this.lstGroupsToSelect_SizeChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "#";
            this.columnHeader1.Width = 50;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 140;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.Silver;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 344);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(979, 6);
            this.splitter1.TabIndex = 2;
            this.splitter1.TabStop = false;
            // 
            // pnlOutput
            // 
            this.pnlOutput.Controls.Add(this.tabControlResult);
            this.pnlOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlOutput.Location = new System.Drawing.Point(0, 350);
            this.pnlOutput.Name = "pnlOutput";
            this.pnlOutput.Size = new System.Drawing.Size(979, 313);
            this.pnlOutput.TabIndex = 3;
            // 
            // tabControlResult
            // 
            this.tabControlResult.Controls.Add(this.tabPageMatch);
            this.tabControlResult.Controls.Add(this.tabPageReplace);
            this.tabControlResult.Controls.Add(this.tabPageSplit);
            this.tabControlResult.Controls.Add(this.tabPageIsMatch);
            this.tabControlResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlResult.ItemSize = new System.Drawing.Size(120, 21);
            this.tabControlResult.Location = new System.Drawing.Point(0, 0);
            this.tabControlResult.Multiline = true;
            this.tabControlResult.Name = "tabControlResult";
            this.tabControlResult.SelectedIndex = 0;
            this.tabControlResult.Size = new System.Drawing.Size(979, 313);
            this.tabControlResult.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControlResult.TabIndex = 0;
            // 
            // tabPageMatch
            // 
            this.tabPageMatch.Controls.Add(this.gboxMatches);
            this.tabPageMatch.Controls.Add(this.splitterGroups);
            this.tabPageMatch.Controls.Add(this.pnlMatchDetail);
            this.tabPageMatch.Controls.Add(this.lblShowHideButton);
            this.tabPageMatch.Location = new System.Drawing.Point(4, 25);
            this.tabPageMatch.Name = "tabPageMatch";
            this.tabPageMatch.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMatch.Size = new System.Drawing.Size(971, 284);
            this.tabPageMatch.TabIndex = 0;
            this.tabPageMatch.Text = "Match();";
            this.tabPageMatch.UseVisualStyleBackColor = true;
            // 
            // gboxMatches
            // 
            this.gboxMatches.Controls.Add(this.lstMatches);
            this.gboxMatches.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gboxMatches.Location = new System.Drawing.Point(3, 3);
            this.gboxMatches.Name = "gboxMatches";
            this.gboxMatches.Size = new System.Drawing.Size(637, 278);
            this.gboxMatches.TabIndex = 3;
            this.gboxMatches.TabStop = false;
            this.gboxMatches.Text = "Matches";
            // 
            // lstMatches
            // 
            this.lstMatches.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.lstMatches.ContextMenuStrip = this.contextMenuStripForCopy;
            this.lstMatches.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstMatches.FullRowSelect = true;
            this.lstMatches.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstMatches.HideSelection = false;
            this.lstMatches.Location = new System.Drawing.Point(3, 16);
            this.lstMatches.Name = "lstMatches";
            this.lstMatches.Size = new System.Drawing.Size(631, 259);
            this.lstMatches.TabIndex = 0;
            this.lstMatches.UseCompatibleStateImageBehavior = false;
            this.lstMatches.View = System.Windows.Forms.View.Details;
            this.lstMatches.SelectedIndexChanged += new System.EventHandler(this.lstMatches_SelectedIndexChanged);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "#";
            this.columnHeader3.Width = 45;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Index";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Value";
            this.columnHeader5.Width = 179;
            // 
            // contextMenuStripForCopy
            // 
            this.contextMenuStripForCopy.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuCopyValue,
            this.toolStripMenuItem1,
            this.mnuCopyAsTabbedText,
            this.mnuCopyAsCsv,
            this.toolStripMenuItem2,
            this.mnuSaveAsCsvSysDefault,
            this.mnuSaveAsCsvUtf8});
            this.contextMenuStripForCopy.Name = "contextMenuStripForCopy";
            this.contextMenuStripForCopy.Size = new System.Drawing.Size(243, 148);
            this.contextMenuStripForCopy.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStripForCopy_Opening);
            // 
            // mnuCopyValue
            // 
            this.mnuCopyValue.Name = "mnuCopyValue";
            this.mnuCopyValue.Size = new System.Drawing.Size(242, 22);
            this.mnuCopyValue.Text = "Copy Value";
            this.mnuCopyValue.Click += new System.EventHandler(this.mnuCopyValue_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(239, 6);
            // 
            // mnuCopyAsTabbedText
            // 
            this.mnuCopyAsTabbedText.Name = "mnuCopyAsTabbedText";
            this.mnuCopyAsTabbedText.Size = new System.Drawing.Size(242, 22);
            this.mnuCopyAsTabbedText.Text = "Copy all/selected as tabbed text";
            this.mnuCopyAsTabbedText.Click += new System.EventHandler(this.mnuCopyAsTabbedText_Click);
            // 
            // mnuCopyAsCsv
            // 
            this.mnuCopyAsCsv.Name = "mnuCopyAsCsv";
            this.mnuCopyAsCsv.Size = new System.Drawing.Size(242, 22);
            this.mnuCopyAsCsv.Text = "Copy all/selected as CSV";
            this.mnuCopyAsCsv.Click += new System.EventHandler(this.mnuCopyAsCsv_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(239, 6);
            // 
            // mnuSaveAsCsvSysDefault
            // 
            this.mnuSaveAsCsvSysDefault.Name = "mnuSaveAsCsvSysDefault";
            this.mnuSaveAsCsvSysDefault.Size = new System.Drawing.Size(242, 22);
            this.mnuSaveAsCsvSysDefault.Text = "Save all/selected as CSV (...)";
            this.mnuSaveAsCsvSysDefault.Click += new System.EventHandler(this.mnuSaveAsCsv_Click);
            // 
            // mnuSaveAsCsvUtf8
            // 
            this.mnuSaveAsCsvUtf8.Name = "mnuSaveAsCsvUtf8";
            this.mnuSaveAsCsvUtf8.Size = new System.Drawing.Size(242, 22);
            this.mnuSaveAsCsvUtf8.Text = "Save all/selected as CSV (UTF-8)";
            this.mnuSaveAsCsvUtf8.Click += new System.EventHandler(this.mnuSaveAsCsvUtf8_Click);
            // 
            // splitterGroups
            // 
            this.splitterGroups.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitterGroups.Location = new System.Drawing.Point(640, 3);
            this.splitterGroups.Name = "splitterGroups";
            this.splitterGroups.Size = new System.Drawing.Size(3, 278);
            this.splitterGroups.TabIndex = 1;
            this.splitterGroups.TabStop = false;
            // 
            // pnlMatchDetail
            // 
            this.pnlMatchDetail.Controls.Add(this.groupBox4);
            this.pnlMatchDetail.Controls.Add(this.statusStripForCaptures);
            this.pnlMatchDetail.Controls.Add(this.splitter5);
            this.pnlMatchDetail.Controls.Add(this.groupBox3);
            this.pnlMatchDetail.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlMatchDetail.Location = new System.Drawing.Point(643, 3);
            this.pnlMatchDetail.Name = "pnlMatchDetail";
            this.pnlMatchDetail.Size = new System.Drawing.Size(304, 278);
            this.pnlMatchDetail.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lstCaptures);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(0, 124);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(304, 130);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Captures";
            // 
            // lstCaptures
            // 
            this.lstCaptures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader9,
            this.columnHeader11});
            this.lstCaptures.ContextMenuStrip = this.contextMenuStripForCopy;
            this.lstCaptures.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstCaptures.FullRowSelect = true;
            this.lstCaptures.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstCaptures.HideSelection = false;
            this.lstCaptures.Location = new System.Drawing.Point(3, 16);
            this.lstCaptures.MultiSelect = false;
            this.lstCaptures.Name = "lstCaptures";
            this.lstCaptures.Size = new System.Drawing.Size(298, 111);
            this.lstCaptures.TabIndex = 0;
            this.lstCaptures.UseCompatibleStateImageBehavior = false;
            this.lstCaptures.View = System.Windows.Forms.View.Details;
            this.lstCaptures.SelectedIndexChanged += new System.EventHandler(this.lstCaptures_SelectedIndexChanged);
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "#";
            this.columnHeader9.Width = 34;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Value";
            this.columnHeader11.Width = 200;
            // 
            // statusStripForCaptures
            // 
            this.statusStripForCaptures.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblCaptureCount,
            this.lblCapturePosition});
            this.statusStripForCaptures.Location = new System.Drawing.Point(0, 254);
            this.statusStripForCaptures.Name = "statusStripForCaptures";
            this.statusStripForCaptures.Size = new System.Drawing.Size(304, 24);
            this.statusStripForCaptures.SizingGrip = false;
            this.statusStripForCaptures.TabIndex = 3;
            this.statusStripForCaptures.Text = "statusStrip2";
            // 
            // lblCaptureCount
            // 
            this.lblCaptureCount.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblCaptureCount.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.lblCaptureCount.Name = "lblCaptureCount";
            this.lblCaptureCount.Size = new System.Drawing.Size(20, 19);
            this.lblCaptureCount.Text = "...";
            // 
            // lblCapturePosition
            // 
            this.lblCapturePosition.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblCapturePosition.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.lblCapturePosition.Name = "lblCapturePosition";
            this.lblCapturePosition.Size = new System.Drawing.Size(269, 19);
            this.lblCapturePosition.Spring = true;
            this.lblCapturePosition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // splitter5
            // 
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter5.Location = new System.Drawing.Point(0, 121);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(304, 3);
            this.splitter5.TabIndex = 1;
            this.splitter5.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lstGroups);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(304, 121);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Groups";
            // 
            // lstGroups
            // 
            this.lstGroups.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader12,
            this.columnHeader8});
            this.lstGroups.ContextMenuStrip = this.contextMenuStripForCopy;
            this.lstGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstGroups.FullRowSelect = true;
            this.lstGroups.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstGroups.HideSelection = false;
            this.lstGroups.Location = new System.Drawing.Point(3, 16);
            this.lstGroups.MultiSelect = false;
            this.lstGroups.Name = "lstGroups";
            this.lstGroups.Size = new System.Drawing.Size(298, 102);
            this.lstGroups.TabIndex = 0;
            this.lstGroups.UseCompatibleStateImageBehavior = false;
            this.lstGroups.View = System.Windows.Forms.View.Details;
            this.lstGroups.SelectedIndexChanged += new System.EventHandler(this.lstGroups_SelectedIndexChanged);
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "#";
            this.columnHeader6.Width = 37;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Name";
            this.columnHeader7.Width = 55;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Captures";
            this.columnHeader12.Width = 73;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Value";
            this.columnHeader8.Width = 83;
            // 
            // lblShowHideButton
            // 
            this.lblShowHideButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblShowHideButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblShowHideButton.Location = new System.Drawing.Point(947, 3);
            this.lblShowHideButton.Name = "lblShowHideButton";
            this.lblShowHideButton.Size = new System.Drawing.Size(21, 278);
            this.lblShowHideButton.TabIndex = 4;
            this.lblShowHideButton.Text = "<\r\n\r\r\n>";
            this.lblShowHideButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblShowHideButton.Click += new System.EventHandler(this.lblShowHideButton_Click);
            this.lblShowHideButton.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lblShowHideButton_MouseMove);
            // 
            // tabPageReplace
            // 
            this.tabPageReplace.Controls.Add(this.groupBox6);
            this.tabPageReplace.Controls.Add(this.groupBox5);
            this.tabPageReplace.Location = new System.Drawing.Point(4, 25);
            this.tabPageReplace.Name = "tabPageReplace";
            this.tabPageReplace.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageReplace.Size = new System.Drawing.Size(971, 284);
            this.tabPageReplace.TabIndex = 1;
            this.tabPageReplace.Text = "Replace();";
            this.tabPageReplace.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtReplaceResult);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(3, 48);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(965, 233);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Replace result";
            // 
            // txtReplaceResult
            // 
            this.txtReplaceResult.ContextMenuStrip = this.contextMenuOnlyCopy;
            this.txtReplaceResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtReplaceResult.Location = new System.Drawing.Point(3, 16);
            this.txtReplaceResult.Name = "txtReplaceResult";
            this.txtReplaceResult.Size = new System.Drawing.Size(959, 214);
            this.txtReplaceResult.TabIndex = 0;
            this.txtReplaceResult.Text = "";
            this.txtReplaceResult.WordWrap = false;
            // 
            // contextMenuOnlyCopy
            // 
            this.contextMenuOnlyCopy.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuCopyReplace});
            this.contextMenuOnlyCopy.Name = "contextMenuOnlyCopy";
            this.contextMenuOnlyCopy.Size = new System.Drawing.Size(103, 26);
            // 
            // mnuCopyReplace
            // 
            this.mnuCopyReplace.Name = "mnuCopyReplace";
            this.mnuCopyReplace.Size = new System.Drawing.Size(102, 22);
            this.mnuCopyReplace.Text = "Copy";
            this.mnuCopyReplace.Click += new System.EventHandler(this.mnuCopyReplace_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnReplace2);
            this.groupBox5.Controls.Add(this.txtReplacePattern);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(965, 45);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Replace expression";
            // 
            // btnReplace2
            // 
            this.btnReplace2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReplace2.Location = new System.Drawing.Point(859, 16);
            this.btnReplace2.Name = "btnReplace2";
            this.btnReplace2.Size = new System.Drawing.Size(100, 23);
            this.btnReplace2.TabIndex = 1;
            this.btnReplace2.Text = "F6 - Replace();";
            this.btnReplace2.UseVisualStyleBackColor = true;
            this.btnReplace2.Click += new System.EventHandler(this.btnReplace_Click);
            // 
            // txtReplacePattern
            // 
            this.txtReplacePattern.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReplacePattern.Location = new System.Drawing.Point(6, 19);
            this.txtReplacePattern.Name = "txtReplacePattern";
            this.txtReplacePattern.Size = new System.Drawing.Size(847, 20);
            this.txtReplacePattern.TabIndex = 0;
            // 
            // tabPageSplit
            // 
            this.tabPageSplit.Controls.Add(this.groupBox8);
            this.tabPageSplit.Controls.Add(this.splitter6);
            this.tabPageSplit.Controls.Add(this.groupBox7);
            this.tabPageSplit.Location = new System.Drawing.Point(4, 25);
            this.tabPageSplit.Name = "tabPageSplit";
            this.tabPageSplit.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSplit.Size = new System.Drawing.Size(971, 284);
            this.tabPageSplit.TabIndex = 2;
            this.tabPageSplit.Text = "Split();";
            this.tabPageSplit.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.lstSlit);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox8.Location = new System.Drawing.Point(3, 3);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(737, 278);
            this.groupBox8.TabIndex = 2;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "String[] strings = new Regex(...).Split(...);";
            // 
            // lstSlit
            // 
            this.lstSlit.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader10,
            this.columnHeader13});
            this.lstSlit.ContextMenuStrip = this.contextMenuStripForCopy;
            this.lstSlit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstSlit.FullRowSelect = true;
            this.lstSlit.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstSlit.HideSelection = false;
            this.lstSlit.Location = new System.Drawing.Point(3, 16);
            this.lstSlit.MultiSelect = false;
            this.lstSlit.Name = "lstSlit";
            this.lstSlit.Size = new System.Drawing.Size(731, 259);
            this.lstSlit.TabIndex = 1;
            this.lstSlit.UseCompatibleStateImageBehavior = false;
            this.lstSlit.View = System.Windows.Forms.View.Details;
            this.lstSlit.SelectedIndexChanged += new System.EventHandler(this.lstSlit_SelectedIndexChanged);
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "#";
            this.columnHeader10.Width = 35;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Value";
            this.columnHeader13.Width = 245;
            // 
            // splitter6
            // 
            this.splitter6.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter6.Location = new System.Drawing.Point(740, 3);
            this.splitter6.Name = "splitter6";
            this.splitter6.Size = new System.Drawing.Size(3, 278);
            this.splitter6.TabIndex = 1;
            this.splitter6.TabStop = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtSplitElement);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox7.Location = new System.Drawing.Point(743, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(225, 278);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "strings[i]";
            // 
            // txtSplitElement
            // 
            this.txtSplitElement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSplitElement.Location = new System.Drawing.Point(3, 16);
            this.txtSplitElement.Name = "txtSplitElement";
            this.txtSplitElement.Size = new System.Drawing.Size(219, 259);
            this.txtSplitElement.TabIndex = 0;
            this.txtSplitElement.Text = "";
            // 
            // tabPageIsMatch
            // 
            this.tabPageIsMatch.Controls.Add(this.lblIsMatchResult);
            this.tabPageIsMatch.Location = new System.Drawing.Point(4, 25);
            this.tabPageIsMatch.Name = "tabPageIsMatch";
            this.tabPageIsMatch.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageIsMatch.Size = new System.Drawing.Size(971, 284);
            this.tabPageIsMatch.TabIndex = 3;
            this.tabPageIsMatch.Text = "IsMatch();";
            this.tabPageIsMatch.UseVisualStyleBackColor = true;
            // 
            // lblIsMatchResult
            // 
            this.lblIsMatchResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIsMatchResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblIsMatchResult.Location = new System.Drawing.Point(3, 3);
            this.lblIsMatchResult.Name = "lblIsMatchResult";
            this.lblIsMatchResult.Size = new System.Drawing.Size(965, 278);
            this.lblIsMatchResult.TabIndex = 0;
            this.lblIsMatchResult.Text = "...";
            this.lblIsMatchResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip.Location = new System.Drawing.Point(0, 663);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(979, 24);
            this.statusStrip.TabIndex = 4;
            this.statusStrip.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.toolStripStatusLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(20, 19);
            this.toolStripStatusLabel1.Text = "...";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripStatusLabel2.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.toolStripStatusLabel2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(944, 19);
            this.toolStripStatusLabel2.Spring = true;
            this.toolStripStatusLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // sfd
            // 
            this.sfd.DefaultExt = "csv";
            this.sfd.Filter = "*.csv|*.csv|*.*|*.*";
            this.sfd.Title = "Select filename for CSV";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 687);
            this.Controls.Add(this.pnlOutput);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.pnlInput);
            this.Controls.Add(this.mainToolStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(843, 654);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "My regex tuner.NET.";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            pnlButtons.ResumeLayout(false);
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.pnlInput.ResumeLayout(false);
            this.pnlSrcText.ResumeLayout(false);
            this.pnlGroups.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.pnlOutput.ResumeLayout(false);
            this.tabControlResult.ResumeLayout(false);
            this.tabPageMatch.ResumeLayout(false);
            this.gboxMatches.ResumeLayout(false);
            this.contextMenuStripForCopy.ResumeLayout(false);
            this.pnlMatchDetail.ResumeLayout(false);
            this.pnlMatchDetail.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.statusStripForCaptures.ResumeLayout(false);
            this.statusStripForCaptures.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.tabPageReplace.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.contextMenuOnlyCopy.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabPageSplit.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.tabPageIsMatch.ResumeLayout(false);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStrip mainToolStrip;
		private System.Windows.Forms.ToolStripButton btnNewPattern;
		private System.Windows.Forms.ToolStripButton btnNewText;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripButton btnExit;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripButton btnIsMatch;
		private System.Windows.Forms.ToolStripButton btnDoMatch;
		private System.Windows.Forms.ToolStripButton btnSplit;
		private System.Windows.Forms.Panel pnlInput;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Panel pnlOutput;
		private System.Windows.Forms.StatusStrip statusStrip;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
		private System.Windows.Forms.Panel pnlSrcText;
		private System.Windows.Forms.Splitter splitter2;
		private System.Windows.Forms.Panel pnlGroups;
		private System.Windows.Forms.Splitter splitter3;
		private MultiTabTextCtrl ctrlInput;
		private RegexTextCtrl ctrlRegex;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button btnSplit2;
		private System.Windows.Forms.Button btnDoMatch2;
		private System.Windows.Forms.Button btnIsMatch2;
		private System.Windows.Forms.ListView lstGroupsToSelect;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.TabControl tabControlResult;
		private System.Windows.Forms.TabPage tabPageMatch;
		private System.Windows.Forms.TabPage tabPageReplace;
		private System.Windows.Forms.Panel pnlMatchDetail;
		private System.Windows.Forms.Splitter splitterGroups;
		private System.Windows.Forms.ListView lstMatches;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.GroupBox gboxMatches;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Splitter splitter5;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.ListView lstCaptures;
		private System.Windows.Forms.ColumnHeader columnHeader9;
		private System.Windows.Forms.ColumnHeader columnHeader11;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Button btnReplace2;
		private System.Windows.Forms.TextBox txtReplacePattern;
		private System.Windows.Forms.RichTextBox txtReplaceResult;
		private System.Windows.Forms.TabPage tabPageSplit;
		private System.Windows.Forms.GroupBox groupBox8;
		private System.Windows.Forms.Splitter splitter6;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.ListView lstSlit;
		private System.Windows.Forms.ColumnHeader columnHeader10;
		private System.Windows.Forms.ColumnHeader columnHeader13;
		private System.Windows.Forms.RichTextBox txtSplitElement;
		private System.Windows.Forms.ListView lstGroups;
		private System.Windows.Forms.ColumnHeader columnHeader6;
		private System.Windows.Forms.ColumnHeader columnHeader7;
		private System.Windows.Forms.ColumnHeader columnHeader12;
		private System.Windows.Forms.ColumnHeader columnHeader8;
		private System.Windows.Forms.StatusStrip statusStripForCaptures;
		private System.Windows.Forms.ToolStripStatusLabel lblCaptureCount;
		private System.Windows.Forms.ToolStripStatusLabel lblCapturePosition;
		private System.Windows.Forms.Label lblShowHideButton;
		private System.Windows.Forms.ToolStripButton btnReplace;
		private System.Windows.Forms.ContextMenuStrip contextMenuStripForCopy;
		private System.Windows.Forms.ToolStripMenuItem mnuCopyValue;
		private System.Windows.Forms.TabPage tabPageIsMatch;
		private System.Windows.Forms.Label lblIsMatchResult;
		private System.Windows.Forms.ToolStripButton btnHelp;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem mnuCopyAsTabbedText;
		private System.Windows.Forms.ToolStripMenuItem mnuCopyAsCsv;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem mnuSaveAsCsvSysDefault;
		private System.Windows.Forms.SaveFileDialog sfd;
		private System.Windows.Forms.ToolStripMenuItem mnuSaveAsCsvUtf8;
		private System.Windows.Forms.ContextMenuStrip contextMenuOnlyCopy;
		private System.Windows.Forms.ToolStripMenuItem mnuCopyReplace;
	}
}

