﻿namespace MyRegexTuner
{
	partial class RegexTextCtrl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.multiTabTextCtrl = new MyRegexTuner.MultiTabTextCtrl();
			this.panel1 = new System.Windows.Forms.Panel();
			this.regexStyleCtrl = new MyRegexTuner.RegexStyleCtrl();
			this.lblStatus = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// multiTabTextCtrl
			// 
			this.multiTabTextCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.multiTabTextCtrl.HeaderText = "label1";
			this.multiTabTextCtrl.Location = new System.Drawing.Point(0, 0);
			this.multiTabTextCtrl.Name = "multiTabTextCtrl";
			this.multiTabTextCtrl.Size = new System.Drawing.Size(693, 306);
			this.multiTabTextCtrl.TabIndex = 1;
			this.multiTabTextCtrl.TabPrefix = null;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.regexStyleCtrl);
			this.panel1.Controls.Add(this.lblStatus);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 286);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(693, 20);
			this.panel1.TabIndex = 2;
			// 
			// regexStyleCtrl
			// 
			this.regexStyleCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.regexStyleCtrl.Location = new System.Drawing.Point(60, 0);
			this.regexStyleCtrl.Name = "regexStyleCtrl";
			this.regexStyleCtrl.RegexOptions = ((System.Text.RegularExpressions.RegexOptions)((System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline)));
			this.regexStyleCtrl.Size = new System.Drawing.Size(633, 20);
			this.regexStyleCtrl.TabIndex = 1;
			this.regexStyleCtrl.RegexStyleChanged += new System.EventHandler(this.regexStyleCtrl_RegexStyleChanged);
			// 
			// lblStatus
			// 
			this.lblStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblStatus.Dock = System.Windows.Forms.DockStyle.Left;
			this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblStatus.Location = new System.Drawing.Point(0, 0);
			this.lblStatus.Name = "lblStatus";
			this.lblStatus.Size = new System.Drawing.Size(60, 20);
			this.lblStatus.TabIndex = 0;
			this.lblStatus.Text = "Ok!";
			this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// RegexTextCtrl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.multiTabTextCtrl);
			this.Name = "RegexTextCtrl";
			this.Size = new System.Drawing.Size(693, 306);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private MultiTabTextCtrl multiTabTextCtrl;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label lblStatus;
		private RegexStyleCtrl regexStyleCtrl;
	}
}
