﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using MyRegexTuner.Gui;

namespace MyRegexTuner
{
	public partial class RegexStyleCtrl :UserControl
	{
		public RegexStyleCtrl()
		{
			InitializeComponent();
		}

		[Browsable(true)]
		public RegexOptions RegexOptions
		{
			get
			{
				RegexOptions result = RegexOptions.None;

				foreach(var lbl in this.Controls.OfType<StyleLabel>().Where( el => el.IsStyleSelected ))
				{
					result |= lbl.RegexOption;
				}

				return result;
			}
			set
			{
				foreach(var lbl in this.Controls.OfType<StyleLabel>())
				{
					lbl.IsStyleSelected = (value & lbl.RegexOption) == lbl.RegexOption;
				}
			}
		}

		public event EventHandler RegexStyleChanged;

		private void styleLabel_MouseClick(object sender, MouseEventArgs e)
		{
			if(RegexStyleChanged != null)
				RegexStyleChanged( this, EventArgs.Empty );
		}
	}
}
