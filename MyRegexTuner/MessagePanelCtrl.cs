﻿using System;
using System.Windows.Forms;

namespace MyRegexTuner
{
	public partial class MessagePanelCtrl :UserControl
	{
		public MessagePanelCtrl(string message, Action closeAction)
		{
			InitializeComponent();

			this.lblMessage.Text = message;
			btnClose.Click += (object sender, EventArgs aarg) => closeAction();
		}
	}
}
