﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using MyRegexTuner.Csv;
using MyRegexTuner.Gui;
using MyRegexTuner.Storage;

namespace MyRegexTuner
{
	public partial class MainForm :Form
	{
		public MainForm()
		{
			InitializeComponent();
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			this.Text += string.Format( "  Runtime version: {0} on {1}", System.Environment.Version.ToString(), System.Environment.OSVersion );

			SetLstGroupsToSelectColWidth();

			try
			{
				PersistStorage storage = PersistStorage.Load();

				if(storage.regex != null)
					this.ctrlRegex.SetInitialFromStorage( storage.regex );

				if(storage.inputs != null)
					this.ctrlInput.SetInitialFromStorage( storage.inputs );

				if(storage.checkedGroups != null)
					SetInitialCheckedGroups( storage.checkedGroups );
			}
			catch(Exception ex)
			{
				MsgBoxes.ErrorDlg( ex.Message );
			}

			this.ctrlRegex.Focus();
		}

		private void btnExit_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			PersistStorage storage = new PersistStorage();
			storage.regex =  this.ctrlRegex.GetCurrentStateForStorage();
			storage.inputs = this.ctrlInput.GetCurrentStateForStorage();
			storage.checkedGroups = GetCurrentCheckedGroups();

			try
			{
				storage.Save();
			}
			catch(Exception ex)
			{
				MsgBoxes.ErrorDlg( ex.Message );
			}
//#if !DEBUG
//            if(MsgBoxes.QuestionDlg( this, "Really exit?") == DialogResult.No)
//                e.Cancel = true;
//#endif
		}

		private string[] GetCurrentCheckedGroups()
		{
			return lstGroupsToSelect.CheckedItems
				.OfType<ListViewItem>()
				.Select( el => el.Text )
				.ToArray();
		}

		private void SetInitialCheckedGroups(string[] checkedGroups)
		{
			foreach(ListViewItem item in lstGroupsToSelect.Items)
			{
				if(Array.IndexOf(checkedGroups, item.Text) != -1)
					item.Checked = true;
			}
		}

		private void btnNewPattern_Click(object sender, EventArgs e)
		{
			ctrlRegex.AddNewPage();
		}

		private void btnNewText_Click(object sender, EventArgs e)
		{
			ctrlInput.AddNewPage();
		}

		private void lstGroupsToSelect_SizeChanged(object sender, EventArgs e)
		{
			SetLstGroupsToSelectColWidth();
		}

		private void SetLstGroupsToSelectColWidth()
		{
			lstGroupsToSelect.Columns[1].Width = lstGroupsToSelect.ClientRectangle.Width - lstGroupsToSelect.Columns[0].Width;
		}

		private void ctrlRegex_TextChanged(object sender, EventArgs e)
		{
			Regex regex = this.ctrlRegex.GetRegexObject();
			if(regex != null)
			{
				var checkedItemNumbers = lstGroupsToSelect.CheckedItems
					.OfType<ListViewItem>()
					.Select( el => el.Text )
					.ToArray();

				lstGroupsToSelect.Items.Clear();

				var groups = regex.GetGroupNames()
					.ToArray();

				for(int idx = 0; idx < groups.Length; idx++)
				{
					ListViewItem lvi = new ListViewItem(idx.ToString());
					lvi.SubItems.Add( groups[idx] );
					lstGroupsToSelect.Items.Add( lvi );

					if(Array.IndexOf(checkedItemNumbers, lvi.Text) != -1)
						lvi.Checked = true;
				}
			}

			btnSplit.Enabled = btnSplit2.Enabled
			       = btnIsMatch.Enabled = btnIsMatch2.Enabled
			       = btnDoMatch.Enabled = btnDoMatch2.Enabled
				   = btnReplace.Enabled
			       = this.ctrlRegex.IsRegexValid;
		}

		private void MainForm_KeyDown(object sender, KeyEventArgs e)
		{
			if(this.ctrlRegex.IsRegexValid)
			{
				switch(e.KeyCode)
				{
					case Keys.F4:
						btnIsMatch_Click( this, EventArgs.Empty );
						break;
					case Keys.F5:
						btnDoMatch_Click( this, EventArgs.Empty );
						break;
					case Keys.F6:
						btnReplace_Click( this, EventArgs.Empty );
						break;
					case Keys.F7:
						btnSplit_Click( this, EventArgs.Empty );
						break;
				}
			}
		}

		private void btnIsMatch_Click(object sender, EventArgs e)
		{
			using(DisableControl.Instance(btnIsMatch2, btnDoMatch2, btnSplit2))
			using(SetCursor.Wait(this))
			{
				Regex rx = this.ctrlRegex.GetRegexObject();

				if(rx == null)
					return;

				this.tabControlResult.SelectedTab = this.tabPageIsMatch;

				if(rx.IsMatch(this.ctrlInput.Text))
				{
					this.lblIsMatchResult.Text = "Input match the regular expression.";
					this.lblIsMatchResult.ForeColor = SystemColors.ControlText;
				}
				else
				{
					this.lblIsMatchResult.Text = "Input does not match the regular expression.";
					this.lblIsMatchResult.ForeColor = Color.Red;
				}

				//if(rx.IsMatch( this.ctrlInput.Text ))
				//    MsgBoxes.NotifyDlg( this, "Input match the regular expression." );
				//else
				//    MsgBoxes.StopDlg( this, "Input does not match the regular expression." );

				this.btnIsMatch2.Focus();
			}
		}

		private Regex regexToMatch;
		private void btnDoMatch_Click(object sender, EventArgs e)
		{
			using(DisableControl.Instance(btnIsMatch2, btnDoMatch2, btnSplit2))
			using(SetCursor.Wait( this ))
			{
				Action closeAction = () =>
				{
					lstMatches.Show();
					var msgPnl = gboxMatches.Controls
						.OfType<MessagePanelCtrl>()
						.FirstOrDefault();
						
					if(msgPnl != null)
						gboxMatches.Controls.Remove( msgPnl );

					lstMatches.Show();
				};

				closeAction();

				this.tabControlResult.SelectedTab = tabPageMatch;
				this.regexToMatch = this.ctrlRegex.GetRegexObject();
				MatchCollection matchCollection = regexToMatch.Matches(this.ctrlInput.Text);

				FillLstMatch(matchCollection);

				if(matchCollection.Count == 0)
				{
					MessagePanelCtrl msg = new MessagePanelCtrl( "There are no matches.", closeAction )
						{
							Dock = DockStyle.Fill
						};

					lstMatches.Hide();
					gboxMatches.Controls.Add( msg );
				}

				this.btnDoMatch2.Focus();
			}
		}

		private void FillLstMatch(MatchCollection matchCollection)
		{
			lstMatches.Items.Clear();
			lstGroups.Items.Clear();
			lstCaptures.Items.Clear();

			tabPageMatch.Text = string.Format( "Match ({0});", matchCollection.Count );

			string[] checkedGroups = this.lstGroupsToSelect.CheckedItems
				.OfType<ListViewItem>()
				.Select( el => el.SubItems[1].Text )
				.ToArray();

			SetupColumns( lstMatches, checkedGroups );

			int idx = 0;
			foreach(Match match in matchCollection)
			{
				MatchListViewItem lvi = new MatchListViewItem( match, idx );
				idx++;

				if(checkedGroups.Length > 0)
				{
					var subItems = checkedGroups
						.Select( el => match.Groups[el].Value )
						.ToArray();

					lvi.SubItems.AddRange( subItems );
				}

				lstMatches.Items.Add( lvi );
			}
		}

		private static void SetupColumns(ListView lst, string[] checkedGroups)
		{
			const string cnstStrGroupColumnPrefix = "G:";

			ColumnHeader[] groupColumns = lst.Columns
				.OfType<ColumnHeader>()
				.Where( el => el.Text.StartsWith( cnstStrGroupColumnPrefix ) )
				.ToArray();

			foreach(var col in groupColumns)
			{
				lst.Columns.Remove( col );
			}

			var columnsToAdd = checkedGroups
				.Select( el => new ColumnHeader
					{
						Text = cnstStrGroupColumnPrefix + el
					} )
				.ToArray();

			lst.Columns.AddRange( columnsToAdd );
		}

		private void btnSplit_Click(object sender, EventArgs e)
		{
			using(DisableControl.Instance(btnIsMatch2, btnDoMatch2, btnSplit2))
			using(SetCursor.Wait(this))
			{
				this.tabControlResult.SelectedTab = tabPageSplit;

				Regex rx = this.ctrlRegex.GetRegexObject();
				var split = rx.Split( this.ctrlInput.Text );

				tabPageSplit.Text = string.Format("Split ({0});", split.Length);
				lstSlit.Items.Clear();

				var itemsToAdd = split
					.Select( (el, idx) => new ListViewItem( new string[] {idx.ToString(), el} ) )
					.ToArray();

				lstSlit.Items.AddRange( itemsToAdd );

				this.btnSplit2.Focus();
			}
		}

		private void lstSlit_SelectedIndexChanged(object sender, EventArgs e)
		{
			if(lstSlit.SelectedItems.Count > 0)
				this.txtSplitElement.Text = lstSlit.SelectedItems[0].SubItems[1].Text;
			else
				this.txtSplitElement.Clear();
		}

		private void lstMatches_SelectedIndexChanged(object sender, EventArgs e)
		{
			using(SetCursor.Wait(this))
			{
				lstGroups.Items.Clear();
				lstCaptures.Items.Clear();
				lblCapturePosition.Text = string.Empty;
				lblCaptureCount.Text = "...";

				if(lstMatches.SelectedItems.Count > 0)
				{
					MatchListViewItem matchItem = (MatchListViewItem) lstMatches.SelectedItems[0];

					int idx = 0;
					foreach(Group @group in matchItem.Match.Groups)
					{
						if(idx == 0)
						{
							idx++;
							continue;
						}

						GroupListViewItem item = new GroupListViewItem(@group, idx, regexToMatch.GroupNameFromNumber(idx));
						idx++;
						lstGroups.Items.Add(item);
					}
				}

				if(lstMatches.SelectedItems.Count == 1)
					UpdateSelectionInSrcText();
			}
		}

		private void lstGroups_SelectedIndexChanged(object sender, EventArgs e)
		{
			using(SetCursor.Wait(this))
			{
				UpdateSelectionInSrcText();

				lstCaptures.Items.Clear();

				if(lstGroups.SelectedItems.Count > 0)
				{
					GroupListViewItem groupItem = (GroupListViewItem) lstGroups.SelectedItems[0];

					lblCaptureCount.Text = "Count: " + groupItem.RxGroup.Captures.Count;

					int idx = 0;
					foreach(Capture capture in groupItem.RxGroup.Captures)
					{
						ListViewItem lvi = new ListViewItem(idx.ToString());
						lvi.SubItems.Add(capture.Value);
						lvi.Tag = capture;

						idx++;
						lstCaptures.Items.Add(lvi);
					}

					if(lstCaptures.Items.Count > 0)
						lstCaptures.Items[0].Selected = true;
				}
			}
		}

		private void lstCaptures_SelectedIndexChanged(object sender, EventArgs e)
		{
			if(lstCaptures.SelectedItems.Count > 0)
			{
				ListViewItem lvi = lstCaptures.SelectedItems[0];
				Capture capture = (Capture)lvi.Tag;

				lblCapturePosition.Text = string.Format( "Selected capture - Index: {0}, Length: {1}", capture.Index, capture.Length );
			}
			else
				lblCapturePosition.Text = string.Empty;
		}

		private void lblShowHideButton_Click(object sender, EventArgs e)
		{
			pnlMatchDetail.Visible = !pnlMatchDetail.Visible;
			splitterGroups.Visible = pnlMatchDetail.Visible;
		}

		private void lblShowHideButton_MouseMove(object sender, MouseEventArgs e)
		{
			if(! lblShowHideButton.RectangleToScreen(lblShowHideButton.ClientRectangle).Contains(Control.MousePosition))
			{
				if(lblShowHideButton.Capture)
				{
					lblShowHideButton.BackColor = SystemColors.Control;
					lblShowHideButton.BorderStyle = BorderStyle.None;
					lblShowHideButton.Capture = false;
				}
			}
			else
			{
				if(!lblShowHideButton.Capture)
				{
					lblShowHideButton.BackColor = SystemColors.GrayText;
					lblShowHideButton.BorderStyle = BorderStyle.FixedSingle;
					lblShowHideButton.Capture = true;
				}
			}
		}

		private void UpdateSelectionInSrcText()
		{
			TextSelection matchSelection = null;
			if(lstMatches.SelectedItems.Count > 0)
			{
				MatchListViewItem item = (MatchListViewItem)lstMatches.SelectedItems[0];
				matchSelection = new TextSelection
					{
						Start = item.Match.Index,
						Length = item.Match.Length
					};
			}

			TextSelection groupSelection = null;
			if(lstGroups.SelectedItems.Count > 0)
			{
				GroupListViewItem item = (GroupListViewItem)lstGroups.SelectedItems[0];
				groupSelection = new TextSelection()
					{
						Start = item.RxGroup.Index, Length = item.RxGroup.Length
					};
			}

			this.ctrlInput.UpdateSelectionInSrcText( matchSelection, groupSelection);
		}

		private void btnReplace_Click(object sender, EventArgs e)
		{
			using(DisableControl.Instance( btnReplace2 ))
			using(SetCursor.Wait( this ))
			{
				if(this.tabControlResult.SelectedTab != tabPageReplace)
					this.tabControlResult.SelectedTab = tabPageReplace;

				Regex rx = this.ctrlRegex.GetRegexObject();
				this.txtReplaceResult.Text = rx.Replace( this.ctrlInput.Text, this.txtReplacePattern.Text );

				this.btnReplace2.Focus();
			}
		}

		private void mnuCopyValue_Click(object sender, EventArgs e)
		{
		    string value = (string)mnuCopyValue.Tag;
            Clipboard.SetText(value ?? string.Empty, TextDataFormat.UnicodeText);
		}

		private HelpForm helpForm = null;
		private void btnHelp_Click(object sender, EventArgs e)
		{
			if(this.helpForm == null)
			{
				this.helpForm = new HelpForm();
				this.helpForm.Closed += (s, a) =>
				{
					this.helpForm = null;
					this.btnHelp.Checked = false;
				};
				this.helpForm.Show();
			}
			else
				this.helpForm.Close();
		}

		private void contextMenuStripForCopy_Opening(object sender, CancelEventArgs e)
		{
			ListView lst = (ListView)FindForm().ActiveControl;

		    SetupCopyValueMenuItem(lst);

		    mnuSaveAsCsvUtf8.Enabled = mnuSaveAsCsvSysDefault.Enabled = mnuCopyAsCsv.Enabled = mnuCopyAsTabbedText.Enabled = lst.Items.Count > 0;
			mnuCopyValue.Enabled = lst.SelectedItems.Count > 0;

			const string cnstPlaceHolder = "...";
			if(mnuSaveAsCsvSysDefault.Text.Contains( cnstPlaceHolder ))
				mnuSaveAsCsvSysDefault.Text = mnuSaveAsCsvSysDefault.Text.Replace( cnstPlaceHolder, System.Text.Encoding.Default.HeaderName );
		}

	    private void SetupCopyValueMenuItem(ListView lst)
	    {
	        Point mousePos = lst.PointToClient(MousePosition);

	        ListViewItem lvi = GetItemFromPoint(lst, mousePos);

	        if (lvi == null)
	        {
	            mnuCopyValue.Enabled = false;
	            mnuCopyValue.Text = "Copy selected cell value...";
	        }
	        else
	        {
	            var subItem = lvi.GetSubItemAt(mousePos.X, mousePos.Y);
	            mnuCopyValue.Text = string.Format("Copy «{0}»", subItem.Text);
	            mnuCopyValue.Tag = subItem.Text;
	        }
	    }

	    private static ListViewItem GetItemFromPoint(ListView lst, Point pt)
	    {
	        return lst.SelectedItems
                .Cast<ListViewItem>()
                .FirstOrDefault(item => item.Bounds.Contains(pt));
	    }

	    private IEnumerable<ListViewItem> MatchCopySrc()
		{
			ListView lst = (ListView)FindForm().ActiveControl;
			return lst.SelectedItems.Count > 0 ? lst.SelectedItems.Cast<ListViewItem>() : lst.Items.Cast<ListViewItem>();
		}

		private void mnuCopyAsTabbedText_Click(object sender, EventArgs e)
		{
			CopyAsTabbedText( MatchCopySrc() );
		}

		private static void CopyAsTabbedText(IEnumerable<ListViewItem> items)
		{
			StringBuilder sb = new StringBuilder();
			StringWriter wrt = new StringWriter(sb);
			SaveCsvToWriter(wrt, items, new CsvDefines( Environment.NewLine, '\t', '"' ));
			Clipboard.SetText(sb.ToString(), TextDataFormat.UnicodeText);
		}

		private void mnuCopyAsCsv_Click(object sender, EventArgs e)
		{
			CopyAsTabbedCsv( MatchCopySrc() );
		}

		private static void CopyAsTabbedCsv(IEnumerable<ListViewItem> items)
		{
			StringBuilder sb = new StringBuilder();
			StringWriter wrt = new StringWriter(sb);
			SaveCsvToWriter( wrt, items, CsvDefines.ExcelCompatible );
			Clipboard.SetText(sb.ToString(), TextDataFormat.UnicodeText);
		}

		private void mnuSaveAsCsv_Click(object sender, EventArgs e)
		{
			SaveAsTabbedCsv( MatchCopySrc(), Encoding.Default);
		}

		private void mnuSaveAsCsvUtf8_Click(object sender, EventArgs e)
		{
			var src = lstMatches.SelectedItems.Count > 0 ?
				lstMatches.SelectedItems.Cast<ListViewItem>() :
				lstMatches.Items.Cast<ListViewItem>();

			SaveAsTabbedCsv(src, Encoding.UTF8);
		}

		private void SaveAsTabbedCsv(IEnumerable<ListViewItem> items, Encoding encoding)
		{
			if(sfd.ShowDialog(this) == DialogResult.OK)
			{
				using(StreamWriter wrt = new StreamWriter( sfd.FileName, false, encoding ))
				{
					SaveCsvToWriter( wrt, items, CsvDefines.ExcelCompatible );
				}
			}
		}

		private static void SaveCsvToWriter(TextWriter wrt, IEnumerable<ListViewItem> items, CsvDefines csvDefines)
		{
			CsvWriter csv = new CsvWriter(wrt, csvDefines);
			using(wrt)
			{
				foreach(var item in items)
				{
					var subItems = item.SubItems
						.Cast<ListViewItem.ListViewSubItem>()
						.Select(el => el.Text)
						.ToArray();

					csv.WriteLine( subItems );
				}
			}
		}

		private void mnuCopyReplace_Click(object sender, EventArgs e)
		{
			txtReplaceResult.SelectAll();
			txtReplaceResult.Copy();
		}
	}
}
