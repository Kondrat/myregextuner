﻿using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace MyRegexTuner
{
	public partial class HelpForm :Form
	{
		public HelpForm()
		{
			InitializeComponent();
		}

		private void HelpForm_Load(object sender, EventArgs e)
		{
			const string cnstStrHelpPath = @"MyRegexTuner.Resources.help.rtf";
			using(Stream istr = Assembly.GetExecutingAssembly().GetManifestResourceStream( cnstStrHelpPath ))
			{
				this.textBox.LoadFile( istr, RichTextBoxStreamType.RichText );
			}
		}

		private void textBox_KeyDown(object sender, KeyEventArgs e)
		{
			if( e.KeyCode == Keys.Escape)
				this.Close();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
