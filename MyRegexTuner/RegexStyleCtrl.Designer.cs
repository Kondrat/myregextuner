﻿using MyRegexTuner.Gui;

namespace MyRegexTuner
{
	partial class RegexStyleCtrl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.styleLabel7 = new MyRegexTuner.Gui.StyleLabel();
			this.styleLabel3 = new MyRegexTuner.Gui.StyleLabel();
			this.styleLabel5 = new MyRegexTuner.Gui.StyleLabel();
			this.styleLabel2 = new MyRegexTuner.Gui.StyleLabel();
			this.styleLabel6 = new MyRegexTuner.Gui.StyleLabel();
			this.styleLabel1 = new MyRegexTuner.Gui.StyleLabel();
			this.styleLabel4 = new MyRegexTuner.Gui.StyleLabel();
			this.styleLabel8 = new MyRegexTuner.Gui.StyleLabel();
			this.SuspendLayout();
			// 
			// styleLabel7
			// 
			this.styleLabel7.Cursor = System.Windows.Forms.Cursors.Hand;
			this.styleLabel7.Dock = System.Windows.Forms.DockStyle.Left;
			this.styleLabel7.Location = new System.Drawing.Point(490, 0);
			this.styleLabel7.Name = "styleLabel7";
			this.styleLabel7.RegexOption = System.Text.RegularExpressions.RegexOptions.ECMAScript;
			this.styleLabel7.Size = new System.Drawing.Size(75, 27);
			this.styleLabel7.TabIndex = 7;
			this.styleLabel7.Text = "styleLabel7";
			this.styleLabel7.MouseClick += new System.Windows.Forms.MouseEventHandler(this.styleLabel_MouseClick);
			// 
			// styleLabel3
			// 
			this.styleLabel3.Cursor = System.Windows.Forms.Cursors.Hand;
			this.styleLabel3.Dock = System.Windows.Forms.DockStyle.Left;
			this.styleLabel3.Location = new System.Drawing.Point(401, 0);
			this.styleLabel3.Name = "styleLabel3";
			this.styleLabel3.RegexOption = System.Text.RegularExpressions.RegexOptions.ExplicitCapture;
			this.styleLabel3.Size = new System.Drawing.Size(89, 27);
			this.styleLabel3.TabIndex = 8;
			this.styleLabel3.Text = "styleLabel3";
			this.styleLabel3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.styleLabel_MouseClick);
			// 
			// styleLabel5
			// 
			this.styleLabel5.Cursor = System.Windows.Forms.Cursors.Hand;
			this.styleLabel5.Dock = System.Windows.Forms.DockStyle.Left;
			this.styleLabel5.Location = new System.Drawing.Point(263, 0);
			this.styleLabel5.Name = "styleLabel5";
			this.styleLabel5.RegexOption = System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace;
			this.styleLabel5.Size = new System.Drawing.Size(138, 27);
			this.styleLabel5.TabIndex = 5;
			this.styleLabel5.Text = "styleLabel5";
			this.styleLabel5.MouseClick += new System.Windows.Forms.MouseEventHandler(this.styleLabel_MouseClick);
			// 
			// styleLabel2
			// 
			this.styleLabel2.Cursor = System.Windows.Forms.Cursors.Hand;
			this.styleLabel2.Dock = System.Windows.Forms.DockStyle.Left;
			this.styleLabel2.IsStyleSelected = true;
			this.styleLabel2.Location = new System.Drawing.Point(194, 0);
			this.styleLabel2.Name = "styleLabel2";
			this.styleLabel2.RegexOption = System.Text.RegularExpressions.RegexOptions.IgnoreCase;
			this.styleLabel2.Size = new System.Drawing.Size(69, 27);
			this.styleLabel2.TabIndex = 1;
			this.styleLabel2.Text = "styleLabel2";
			this.styleLabel2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.styleLabel_MouseClick);
			// 
			// styleLabel6
			// 
			this.styleLabel6.Cursor = System.Windows.Forms.Cursors.Hand;
			this.styleLabel6.Dock = System.Windows.Forms.DockStyle.Left;
			this.styleLabel6.Location = new System.Drawing.Point(125, 0);
			this.styleLabel6.Name = "styleLabel6";
			this.styleLabel6.RegexOption = System.Text.RegularExpressions.RegexOptions.RightToLeft;
			this.styleLabel6.Size = new System.Drawing.Size(69, 27);
			this.styleLabel6.TabIndex = 12;
			this.styleLabel6.Text = "styleLabel6";
			this.styleLabel6.MouseClick += new System.Windows.Forms.MouseEventHandler(this.styleLabel_MouseClick);
			// 
			// styleLabel1
			// 
			this.styleLabel1.Cursor = System.Windows.Forms.Cursors.Hand;
			this.styleLabel1.Dock = System.Windows.Forms.DockStyle.Left;
			this.styleLabel1.Location = new System.Drawing.Point(69, 0);
			this.styleLabel1.Name = "styleLabel1";
			this.styleLabel1.RegexOption = System.Text.RegularExpressions.RegexOptions.Multiline;
			this.styleLabel1.Size = new System.Drawing.Size(56, 27);
			this.styleLabel1.TabIndex = 11;
			this.styleLabel1.Text = "styleLabel1";
			this.styleLabel1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.styleLabel_MouseClick);
			// 
			// styleLabel4
			// 
			this.styleLabel4.Cursor = System.Windows.Forms.Cursors.Hand;
			this.styleLabel4.Dock = System.Windows.Forms.DockStyle.Left;
			this.styleLabel4.IsStyleSelected = true;
			this.styleLabel4.Location = new System.Drawing.Point(0, 0);
			this.styleLabel4.Name = "styleLabel4";
			this.styleLabel4.RegexOption = System.Text.RegularExpressions.RegexOptions.Singleline;
			this.styleLabel4.Size = new System.Drawing.Size(69, 27);
			this.styleLabel4.TabIndex = 10;
			this.styleLabel4.Text = "styleLabel4";
			this.styleLabel4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.styleLabel_MouseClick);
			// 
			// styleLabel8
			// 
			this.styleLabel8.Cursor = System.Windows.Forms.Cursors.Hand;
			this.styleLabel8.Dock = System.Windows.Forms.DockStyle.Left;
			this.styleLabel8.Location = new System.Drawing.Point(565, 0);
			this.styleLabel8.Name = "styleLabel8";
			this.styleLabel8.RegexOption = System.Text.RegularExpressions.RegexOptions.CultureInvariant;
			this.styleLabel8.Size = new System.Drawing.Size(93, 27);
			this.styleLabel8.TabIndex = 13;
			this.styleLabel8.Text = "styleLabel8";
			// 
			// RegexStyleCtrl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.styleLabel8);
			this.Controls.Add(this.styleLabel7);
			this.Controls.Add(this.styleLabel3);
			this.Controls.Add(this.styleLabel5);
			this.Controls.Add(this.styleLabel2);
			this.Controls.Add(this.styleLabel6);
			this.Controls.Add(this.styleLabel1);
			this.Controls.Add(this.styleLabel4);
			this.Name = "RegexStyleCtrl";
			this.Size = new System.Drawing.Size(732, 27);
			this.ResumeLayout(false);

		}

		#endregion

		private StyleLabel styleLabel2;
		private StyleLabel styleLabel5;
		private StyleLabel styleLabel7;
		private StyleLabel styleLabel3;
		private StyleLabel styleLabel4;
		private StyleLabel styleLabel1;
		private StyleLabel styleLabel6;
		private StyleLabel styleLabel8;


	}
}
