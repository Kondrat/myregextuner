﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyRegexTuner.Gui
{
	class MyToolStripButton :ToolStripButton
	{
		private readonly int number;
		readonly RichTextBox textBox = new RichTextBox();

		public MyToolStripButton(string caption, int number)
			:base(caption + "#" + number.ToString())
		{
			this.number = number;
			this.textBox.Dock = DockStyle.Fill;
			this.textBox.WordWrap = false;
			this.textBox.DetectUrls = false;

			this.textBox.TextChanged += textBox_TextChanged;
		}

		void textBox_TextChanged(object sender, EventArgs e)
		{
			const char cnstSeparator = ':';

			string[] parts = this.Text.Split( cnstSeparator );
			string buttonHeader = parts[0].TrimEnd();

			int len = this.textBox.TextLength;
			if(len > 0)
				this.Text = string.Format( "{0} {1}{2}", buttonHeader, cnstSeparator, len );
			else
				this.Text = buttonHeader;
		}

		public int Number
		{
			get
			{
				return number;
			}
		}

		public RichTextBox TextBox
		{
			get
			{
				return textBox;
			}
		}

		[Browsable(true)]
		public ContextMenuStrip ContextMenuStrip
		{
			get; set;
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Right)
			{
				if(this.ContextMenuStrip != null)
				{
					this.PerformClick();

					int offsetX = this.Parent.Items
						.OfType<MyToolStripButton>()
						.TakeWhile( el => el.Number < this.number )
						.Select( el => el.Width )
						.Sum();

					this.ContextMenuStrip.Show(this.Parent, e.X + offsetX, e.Y);
				}
			}

			base.OnMouseUp(e);
		}
	}
}
