﻿using System.Windows.Forms;

namespace MyRegexTuner.Gui
{
	class BannerRichTextBox:RichTextBox
	{
		protected override void DefWndProc(ref Message msg)
		{
			if(	msg.Msg == (int)WinApi.Msg.WM_LBUTTONDOWN ||
				msg.Msg == (int)WinApi.Msg.WM_LBUTTONDBLCLK ||
				msg.Msg == (int)WinApi.Msg.WM_RBUTTONDOWN ||
				msg.Msg == (int)WinApi.Msg.WM_RBUTTONDBLCLK ||
				msg.Msg == (int)WinApi.Msg.WM_MBUTTONDOWN ||
				msg.Msg == (int)WinApi.Msg.WM_MBUTTONDBLCLK ||
				msg.Msg == (int)WinApi.Msg.WM_CONTEXTMENU ||
				msg.Msg == (int)WinApi.Msg.WM_SETFOCUS
				)
				return;

			base.DefWndProc(ref msg);
		}
	}
}
