﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace MyRegexTuner.Gui
{
	class StyleLabel :Control
	{
		private bool _bIsMouseIsOver = false;

		#region Option

		private RegexOptions _regexOption;

		[Browsable(true), DefaultValue(RegexOptions.None)]
		public RegexOptions RegexOption
		{
			get
			{
				return _regexOption;
			}
			set
			{
				_regexOption = value;
				Invalidate();
			}
		}

		#endregion

		#region IsStyleSelected

		private bool _isStyleSelected;

		[Browsable(true), DefaultValue(false)]
		public bool IsStyleSelected
		{
			get
			{
				return _isStyleSelected;
			}
			set
			{
				_isStyleSelected = value;
				Invalidate();
			}
		}

		#endregion

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);

			string str = RegexOption.ToString();

			using(Brush br = new SolidBrush( IsStyleSelected? SystemColors.ControlText: SystemColors.GrayText ))
			{
				StringFormat sf = new StringFormat(StringFormat.GenericDefault);
				sf.Alignment = StringAlignment.Center;
				sf.LineAlignment = StringAlignment.Center;

				e.Graphics.DrawString( str, Font, br, ClientRectangle, sf );

			    if (_isStyleSelected)
			    {
			        var rect = ClientRectangle;
                    rect.Inflate( -4, -4);
			        e.Graphics.DrawLine(SystemPens.ControlText, rect.X, rect.Bottom, rect.Right, rect.Bottom );
			    }

				if(_bIsMouseIsOver)
				{
					Rectangle rect = ClientRectangle;
					rect.Inflate( -1, -1 );
					e.Graphics.DrawRectangle( SystemPens.ButtonShadow, rect );
				}
			}
		}

		protected override void OnMouseClick(MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Left)
			{
				IsStyleSelected = !IsStyleSelected;
				Invalidate();
			}

			base.OnMouseClick(e);
		}

		protected override void OnMouseEnter(EventArgs e)
		{
			base.OnMouseEnter(e);
			_bIsMouseIsOver = true;
			Invalidate();
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			base.OnMouseLeave(e);
			_bIsMouseIsOver = false;
			Invalidate();
		}
	}
}
