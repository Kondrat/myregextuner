using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace MyRegexTuner.Gui
{
	public class GroupListViewItem: ListViewItem
	{
		private readonly Group group;

		public GroupListViewItem(Group @group, int idx, string groupName)
		{
			this.group = group;

			this.Text = idx.ToString();
			this.SubItems.Add(groupName);
			this.SubItems.Add(group.Captures.Count.ToString() ) ;
			this.SubItems.Add(group.Value);
		}

		public Group RxGroup
		{
			get
			{
				return group;
			}
		}
	}
}