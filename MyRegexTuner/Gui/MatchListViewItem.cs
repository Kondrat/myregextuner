﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace MyRegexTuner.Gui
{
	public class MatchListViewItem: ListViewItem
	{
		private readonly Match match;

		public MatchListViewItem(Match match, int idx)
		{
			this.match = match;

			this.Text = idx.ToString();
			this.SubItems.Add( match.Index.ToString() );
			this.SubItems.Add( match.Value );
		}

		public Match Match
		{
			get
			{
				return match;
			}
		}
	}
}
