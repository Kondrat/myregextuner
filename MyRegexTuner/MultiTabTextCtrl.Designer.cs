﻿using System.Drawing;
using System.Windows.Forms;

namespace MyRegexTuner
{
	partial class MultiTabTextCtrl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MultiTabTextCtrl));
			this.toolStrip = new System.Windows.Forms.ToolStrip();
			this.contextMenuStripForToolBar = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.mnuNew = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuPasteAsNew = new System.Windows.Forms.ToolStripMenuItem();
			this.btnClose = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.btnAddNew = new System.Windows.Forms.ToolStripButton();
			this.contextMenuStripForButton = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.mnuClose = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuCloseAll = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
			this.mnuAddNew = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuPaste = new System.Windows.Forms.ToolStripMenuItem();
			this.pnl = new System.Windows.Forms.Panel();
			this.lblHeader = new System.Windows.Forms.Label();
			this.contextMenuStripForTextBox = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.mnuCopy = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuPaste2 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
			this.mnuClear = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
			this.mnuClear2 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStrip.SuspendLayout();
			this.contextMenuStripForToolBar.SuspendLayout();
			this.contextMenuStripForButton.SuspendLayout();
			this.contextMenuStripForTextBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// toolStrip
			// 
			this.toolStrip.ContextMenuStrip = this.contextMenuStripForToolBar;
			this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnClose,
            this.toolStripSeparator1,
            this.btnAddNew});
			this.toolStrip.Location = new System.Drawing.Point(0, 22);
			this.toolStrip.Name = "toolStrip";
			this.toolStrip.ShowItemToolTips = false;
			this.toolStrip.Size = new System.Drawing.Size(360, 25);
			this.toolStrip.TabIndex = 1;
			this.toolStrip.Text = "toolStrip1";
			// 
			// contextMenuStripForToolBar
			// 
			this.contextMenuStripForToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuNew,
            this.mnuPasteAsNew});
			this.contextMenuStripForToolBar.Name = "contextMenuStripForToolBar";
			this.contextMenuStripForToolBar.Size = new System.Drawing.Size(173, 48);
			this.contextMenuStripForToolBar.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStripForToolBar_Opening);
			// 
			// mnuNew
			// 
			this.mnuNew.Image = global::MyRegexTuner.Properties.Resources.AddPageButton;
			this.mnuNew.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.mnuNew.Name = "mnuNew";
			this.mnuNew.Size = new System.Drawing.Size(172, 22);
			this.mnuNew.Text = "New";
			this.mnuNew.Click += new System.EventHandler(this.mnuAddNew_Click);
			// 
			// mnuPasteAsNew
			// 
			this.mnuPasteAsNew.Name = "mnuPasteAsNew";
			this.mnuPasteAsNew.Size = new System.Drawing.Size(172, 22);
			this.mnuPasteAsNew.Text = "Paste text as new";
			this.mnuPasteAsNew.Click += new System.EventHandler(this.mnuPasteAsNew_Click);
			// 
			// btnClose
			// 
			this.btnClose.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.btnClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnClose.Image = global::MyRegexTuner.Properties.Resources.ClosePageButton;
			this.btnClose.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.btnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(23, 22);
			this.btnClose.Text = "toolStripButton2";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// btnAddNew
			// 
			this.btnAddNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnAddNew.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNew.Image")));
			this.btnAddNew.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnAddNew.Name = "btnAddNew";
			this.btnAddNew.Size = new System.Drawing.Size(23, 22);
			this.btnAddNew.Text = "+";
			this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
			// 
			// contextMenuStripForButton
			// 
			this.contextMenuStripForButton.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuClose,
            this.mnuCloseAll,
            this.toolStripMenuItem1,
            this.mnuAddNew,
            this.mnuPaste,
            this.toolStripMenuItem3,
            this.mnuClear2});
			this.contextMenuStripForButton.Name = "contextMenuStrip";
			this.contextMenuStripForButton.Size = new System.Drawing.Size(153, 148);
			// 
			// mnuClose
			// 
			this.mnuClose.Image = global::MyRegexTuner.Properties.Resources.ClosePageButton;
			this.mnuClose.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.mnuClose.Name = "mnuClose";
			this.mnuClose.Size = new System.Drawing.Size(152, 22);
			this.mnuClose.Text = "Close";
			this.mnuClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// mnuCloseAll
			// 
			this.mnuCloseAll.Name = "mnuCloseAll";
			this.mnuCloseAll.Size = new System.Drawing.Size(152, 22);
			this.mnuCloseAll.Text = "Close All";
			this.mnuCloseAll.Click += new System.EventHandler(this.mnuCloseAll_Click);
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(149, 6);
			// 
			// mnuAddNew
			// 
			this.mnuAddNew.Image = ((System.Drawing.Image)(resources.GetObject("mnuAddNew.Image")));
			this.mnuAddNew.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.mnuAddNew.Name = "mnuAddNew";
			this.mnuAddNew.Size = new System.Drawing.Size(152, 22);
			this.mnuAddNew.Text = "Add new";
			this.mnuAddNew.Click += new System.EventHandler(this.mnuAddNew_Click);
			// 
			// mnuPaste
			// 
			this.mnuPaste.Name = "mnuPaste";
			this.mnuPaste.Size = new System.Drawing.Size(152, 22);
			this.mnuPaste.Text = "Paste";
			this.mnuPaste.Click += new System.EventHandler(this.mnuPaste_Click);
			// 
			// pnl
			// 
			this.pnl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnl.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.pnl.Location = new System.Drawing.Point(0, 47);
			this.pnl.Name = "pnl";
			this.pnl.Size = new System.Drawing.Size(360, 223);
			this.pnl.TabIndex = 0;
			// 
			// lblHeader
			// 
			this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
			this.lblHeader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblHeader.Location = new System.Drawing.Point(0, 0);
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Size = new System.Drawing.Size(360, 22);
			this.lblHeader.TabIndex = 2;
			this.lblHeader.Text = "label1";
			this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// contextMenuStripForTextBox
			// 
			this.contextMenuStripForTextBox.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuCopy,
            this.mnuPaste2,
            this.toolStripMenuItem2,
            this.mnuClear});
			this.contextMenuStripForTextBox.Name = "contextMenuStripForTextBox";
			this.contextMenuStripForTextBox.Size = new System.Drawing.Size(113, 76);
			// 
			// mnuCopy
			// 
			this.mnuCopy.Name = "mnuCopy";
			this.mnuCopy.Size = new System.Drawing.Size(112, 22);
			this.mnuCopy.Text = "Copy";
			this.mnuCopy.Click += new System.EventHandler(this.mnuCopy_Click);
			// 
			// mnuPaste2
			// 
			this.mnuPaste2.Name = "mnuPaste2";
			this.mnuPaste2.Size = new System.Drawing.Size(112, 22);
			this.mnuPaste2.Text = "Paste";
			this.mnuPaste2.Click += new System.EventHandler(this.mnuPaste_Click);
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(109, 6);
			// 
			// mnuClear
			// 
			this.mnuClear.Name = "mnuClear";
			this.mnuClear.Size = new System.Drawing.Size(112, 22);
			this.mnuClear.Text = "Clear";
			this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// toolStripMenuItem3
			// 
			this.toolStripMenuItem3.Name = "toolStripMenuItem3";
			this.toolStripMenuItem3.Size = new System.Drawing.Size(149, 6);
			// 
			// mnuClear2
			// 
			this.mnuClear2.Name = "mnuClear2";
			this.mnuClear2.Size = new System.Drawing.Size(152, 22);
			this.mnuClear2.Text = "Clear";
			this.mnuClear2.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// MultiTabTextCtrl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.pnl);
			this.Controls.Add(this.toolStrip);
			this.Controls.Add(this.lblHeader);
			this.Name = "MultiTabTextCtrl";
			this.Size = new System.Drawing.Size(360, 270);
			this.Load += new System.EventHandler(this.MultiTabTextCtrl_Load);
			this.toolStrip.ResumeLayout(false);
			this.toolStrip.PerformLayout();
			this.contextMenuStripForToolBar.ResumeLayout(false);
			this.contextMenuStripForButton.ResumeLayout(false);
			this.contextMenuStripForTextBox.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel pnl;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripButton btnClose;
		private System.Windows.Forms.ContextMenuStrip contextMenuStripForButton;
		private System.Windows.Forms.ToolStripMenuItem mnuClose;
		private System.Windows.Forms.ToolStripMenuItem mnuCloseAll;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem mnuAddNew;
		private System.Windows.Forms.ToolStripButton btnAddNew;
		private System.Windows.Forms.ToolStripMenuItem mnuPaste;
		private System.Windows.Forms.Label lblHeader;
		private System.Windows.Forms.ContextMenuStrip contextMenuStripForTextBox;
		private System.Windows.Forms.ToolStripMenuItem mnuCopy;
		private System.Windows.Forms.ToolStripMenuItem mnuPaste2;
		private System.Windows.Forms.ToolStrip toolStrip;
		private ContextMenuStrip contextMenuStripForToolBar;
		private ToolStripMenuItem mnuNew;
		private ToolStripMenuItem mnuPasteAsNew;
		private ToolStripSeparator toolStripMenuItem2;
		private ToolStripMenuItem mnuClear;
		private ToolStripSeparator toolStripMenuItem3;
		private ToolStripMenuItem mnuClear2;

	}
}
