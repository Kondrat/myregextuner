﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace MyRegexTuner.Storage
{
	public class PersistStorage
	{
		#region IO

		private const string cnstStrFileName = "autosave.xml";

		private static string GetFileName()
		{
			return Path.Combine( Path.GetDirectoryName( Application.ExecutablePath), cnstStrFileName );
		}

		public static PersistStorage Load()
		{
			string fileName = GetFileName();
			if(! File.Exists( fileName ))
				return new PersistStorage();

			try
			{
				XmlSerializer ser = new XmlSerializer( typeof(PersistStorage) );
				using(FileStream ifs = new FileStream( fileName, FileMode.Open, FileAccess.Read ))
				{
					PersistStorage data = (PersistStorage)ser.Deserialize( ifs );
					return data;
				}
			}
			catch(Exception)
			{
				return new PersistStorage();
			}
		}

		public void Save()
		{
			XmlSerializer ser = new XmlSerializer( typeof(PersistStorage) );
			using(FileStream ofs = new FileStream(GetFileName(), FileMode.Create, FileAccess.Write))
			{
				ser.Serialize(ofs, this);
			}
		}

		#endregion

		public RegexStorage regex;

		public StorageElementForInputText[] inputs;

		public string[] checkedGroups;
	}
}
