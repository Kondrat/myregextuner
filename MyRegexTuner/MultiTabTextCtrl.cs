﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MyRegexTuner.Gui;
using MyRegexTuner.Storage;

namespace MyRegexTuner
{
	public partial class MultiTabTextCtrl :UserControl
	{
		private readonly Font fntGroupSelection;
		private readonly Font fntMatchSelection;

		public MultiTabTextCtrl()
		{
			InitializeComponent();

			this.fntGroupSelection = new Font(pnl.Font, FontStyle.Bold | FontStyle.Underline);
			this.fntMatchSelection = new Font(pnl.Font, FontStyle.Bold);
		}

		private void MultiTabTextCtrl_Load(object sender, EventArgs e)
		{
			AddNewPage();
		}

		public void AddNewPage()
		{
			MyToolStripButton btn = AddNewPageInternal(GetMaxUsedNumber() + 1, string.Empty);
			SetAsCurrent(btn);
		}

		private MyToolStripButton AddNewPageInternal(int number, string text)
		{
			MyToolStripButton btn = new MyToolStripButton(this.TabPrefix, number);
			btn.Click += btn_Click;

			pnl.Controls.Add( btn.TextBox );
			btn.TextBox.ContextMenuStrip = this.contextMenuStripForTextBox;
			btn.TextBox.Text = text;

			int idx = toolStrip.Items.IndexOf( btnAddNew );
			this.toolStrip.Items.Insert( idx, btn );

			btn.ContextMenuStrip = this.contextMenuStripForButton;

			return btn;
		}

		private int GetMaxUsedNumber()
		{
			var list = this.toolStrip.Items
				.OfType<MyToolStripButton>()
				.Select( el => el.Number )
				.ToArray();

			return list.Length > 0? list.Max(): 0;
		}

		[Browsable(true)]
		public new event EventHandler TextChanged;

		private void RaiseTextChanged(object sender, EventArgs e)
		{
			if(this.TextChanged != null)
				this.TextChanged( this, EventArgs.Empty );
		}

		private void SetAsCurrent(MyToolStripButton btn)
		{
			using(UpdateFixer.Instance( pnl ))
			{
				SetAsChecked( btn );

				foreach(RichTextBox textBox in pnl.Controls.OfType<RichTextBox>())
				{
					textBox.TextChanged -= RaiseTextChanged;
					textBox.Hide();
				}

				btn.TextBox.Show();
				btn.TextBox.TextChanged += RaiseTextChanged;

				RaiseTextChanged( this, EventArgs.Empty );
			}
			btn.TextBox.Focus();
		}

		private void SetAsChecked(MyToolStripButton btn)
		{
			foreach(var item in this.toolStrip.Items.OfType<MyToolStripButton>())
			{
				item.Checked = false;
			}

			btn.Checked = true;
		}

		void btn_Click(object sender, EventArgs e)
		{
			SetAsCurrent((MyToolStripButton)sender);
		}

		internal MyToolStripButton SelectedButton
		{
			get
			{
				return this.toolStrip.Items.OfType<MyToolStripButton>()
					.FirstOrDefault( el => el.Checked );
			}
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			using(UpdateFixer.Instance( pnl ))
			{
				MyToolStripButton selected = SelectedButton;
				pnl.Controls.Remove( selected.TextBox );

				MyToolStripButton nextBtn = FindNextBtn( selected );

				selected.Click -= btn_Click;
				toolStrip.Items.Remove( selected );

				if(nextBtn != null)
					SetAsCurrent( nextBtn );
				else
					AddNewPage();
			}
		}

		private MyToolStripButton FindNextBtn(MyToolStripButton selected)
		{
			var buttons = this.toolStrip.Items
				.OfType<MyToolStripButton>()
				.ToArray();

			int idxSelected = Array.IndexOf( buttons, selected);
			int idxNext = 0;

			if(idxSelected == 0)
				idxNext = 1;
			else
				idxNext = idxSelected == buttons.Length - 1 ? idxSelected - 1 : idxSelected + 1;

			return idxNext < buttons.Length? buttons[idxNext]: null;
		}

		private void mnuAddNew_Click(object sender, EventArgs e)
		{
			AddNewPage();
		}

		private void mnuCloseAll_Click(object sender, EventArgs e)
		{
			using(UpdateFixer.Instance( pnl ))
			{
				CloseAllInternal();
				AddNewPage();
			}
		}

		private void CloseAllInternal()
		{
			foreach(Control control in this.pnl.Controls)
			{
				control.TextChanged -= RaiseTextChanged;
			}
			this.pnl.Controls.Clear();

			var toDelete = this.toolStrip.Items
				.OfType<MyToolStripButton>()
				.ToArray();
			toDelete.ForEachDo( el =>
			{
				el.Click -= btn_Click;
				this.toolStrip.Items.Remove( el );
			} );
		}

		private void btnAddNew_Click(object sender, EventArgs e)
		{
			AddNewPage();
		}

		[Browsable(true)]
		public string TabPrefix
		{
			get; set;
		}

		public new string Text
		{
			get
			{
				return SelectedButton.TextBox.Text;
			}
		}

		[Browsable(true)]
		public string HeaderText
		{
			get
			{
				return lblHeader.Text;
			}
			set
			{
				lblHeader.Text = value;
			}
		}

		private void mnuPaste_Click(object sender, EventArgs e)
		{
			this.SelectedButton.TextBox.Text = Clipboard.GetText( TextDataFormat.Text );
		}

		private void mnuCopy_Click(object sender, EventArgs e)
		{
			RichTextBox textBox = this.SelectedButton.TextBox;

			if(textBox.SelectionLength > 0)
				Clipboard.SetDataObject( textBox.SelectedText, true );
			else
				Clipboard.SetDataObject(textBox.Text, true);
		}

		public StorageElementForInputText[] GetCurrentStateForStorage()
		{
			return this.toolStrip.Items
				.OfType<MyToolStripButton>()
				.Select(el => new StorageElementForInputText
				{
					text = el.TextBox.Text,
					number = el.Number,
					isSelected = el.Checked
				})
				.ToArray();
		}

		public void SetInitialFromStorage(StorageElementForInputText[] elements)
		{
			using(UpdateFixer.Instance( this ))
			{
				if(elements.Length > 0)
				{
					CloseAllInternal();

					foreach(var element in elements)
					{
						MyToolStripButton btn = AddNewPageInternal(element.number, element.text);
						if(element.isSelected)
							SetAsCurrent(btn);
					}
				}
			}
		}

		public void UpdateSelectionInSrcText(TextSelection matchSelection, TextSelection groupSelection)
		{
			RichTextBox tb = this.SelectedButton.TextBox;

			int selectionStart = tb.SelectionStart;
			tb.SelectAll();
			tb.SelectionColor = SystemColors.ControlText;
			tb.SelectionFont = pnl.Font;
			tb.Select( selectionStart, 0 );

			TrySelectRange(tb, matchSelection, Color.MediumBlue, this.fntMatchSelection);
			TrySelectRange(tb, groupSelection, Color.Red, this.fntGroupSelection);

			Control prevFocus = this.FindForm().ActiveControl;
			tb.Focus();
			prevFocus.Focus();
		}

		private static void TrySelectRange(RichTextBox tb, TextSelection range, Color color, Font font)
		{
			if(range != null)
			{
				tb.Select(range.Start, range.Length);
				tb.SelectionColor = color;
				tb.SelectionFont = font;
			}
		}

		private void contextMenuStripForToolBar_Opening(object sender, CancelEventArgs e)
		{
			mnuPasteAsNew.Enabled = Clipboard.ContainsText();
		}

		private void mnuPasteAsNew_Click(object sender, EventArgs e)
		{
			AddNewPage();
			this.SelectedButton.TextBox.Text = Clipboard.GetText();
		}

		private void mnuClear_Click(object sender, EventArgs e)
		{
			this.SelectedButton.TextBox.Clear();
		}
	}
}
